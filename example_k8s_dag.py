import copy
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

affinity = {
    'nodeAffinity': {
        'requiredDuringSchedulingIgnoredDuringExecution': {
            'nodeSelectorTerms': [
                {
                    'matchExpressions': [
                        {'key': 'node-role.kubernetes.io/compute-prod',
                         'operator': 'Exists'}
                    ]
                }
            ]
        }
    }
}

volumes = [
    {
        "name": "data",
        "persistentVolumeClaim": {"claimName": "data"},
    },
    {
        "name": "yarn",
        "configMap": {
            "name": "yarn-config-map",
            "defaultMode": '365',
        },
    },
    {
        "name": "spark",
        "configMap": {
            "name": "spark-config-map",
            "defaultMode": '365',
        },
    },
    {
        "name": "hive",
        "configMap": {
            "name": "hive-config-map",
            "defaultMode": '365',
        },
    },
    {
        "name": "keytab",
        "secret": {"secretName": "keytab-secret"},
    },
    {
        "name": "cfg",
        "configMap": {
            "name": "config-for-spark",
            "defaultMode": '365',
        },
    },
]

volume_mounts = [
    {
        "mountPath": "/data",
        "name": "data",
    },
    {
        "mountPath": "/etc/hadoop/conf",
        "name": "yarn",
    },
    {
        "mountPath": "/etc/hive/conf",
        "name": "hive",
    },
    {
        "mountPath": "/etc/spark2/conf",
        "name": "spark",
    },
    {
        "mountPath": "/etc/spark2/conf/yarn-conf",
        "name": "hive",
    },
    {
        "mountPath": "/keytabs",
        "name": "keytab",
    },
    {
        "mountPath": "/opt/openshift.cfg",
        "subPath": "openshift.cfg",
        "name": "cfg",
    },
]

executor_config = {
    "KubernetesExecutor": {
        "image": "airflow/ci:latest",
        "volumes": volumes,
        "volume_mounts": volume_mounts,
        "affinity": affinity
    }
}


def task1(ts):
    print('Task1. TS:', ts)
    for i in range(20):
        print(i)

def task2(ts):
    print('Task2. TS:', ts)
    for i in range(20):
        print(i**2)

executor_config_task2 = copy.deepcopy(executor_config)
executor_config_task2['KubernetesExecutor']['image'] = "airflow/ci:stable"

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2021, 7, 19),
    'retries': 3,
    'retry_delay': timedelta(minutes=1),
    'executor_config': executor_config,
}

dag = DAG(
    'dag_name',
    catchup=False,
    concurrency=3,
    schedule_interval='*/5 * * * *',
    default_args=default_args
)

task_1 = PythonOperator(
    task_id='task1',
    python_callable=task1,
    op_kwargs={'{{ts}}'},
    dag=dag
)

task_2 = PythonOperator(
    task_id='task2',
    python_callable=task2,
    executor_config=executor_config_task2,
    op_kwargs={'{{ts}}'},
    dag=dag
)
